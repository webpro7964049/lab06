import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return {
      celsius: parseFloat(celsius),
      farenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    };
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.temperatureService.convert(celsius);
  }
}
